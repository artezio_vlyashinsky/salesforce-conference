public class Interviewers {
   @InvocableMethod(label='Assign Interviewers' description='Retun apropiate interviwers for this session.')
   public static void assignInterviewers (List<Interview__c> interviewList ) {
       //List<Interview__c> interviewList     
       Interview__c interview = interviewList[0];       
       //Interview__c interview = [SELECT Id, Name, Session__c FROM Interview__c Limit 1];
       Session__c session = [SELECT Id, Name, Technologies__c FROM Session__c WHERE Id=:interview.Session__c];
       String[] technologyList = session.Technologies__c.split(';');
       String[] requiredTopics = technologyList.clone();
       List<Interviewer__c> interviewerList = [SELECT Id, Name, Technologies__c FROM Interviewer__c];
       Integer interviewerQuantity = 0;
       for(Interviewer__c interviewer : interviewerList) {
           Boolean topicIsKnown = false;
           for (String technology : technologyList) {
               if(interviewer.Technologies__c.contains(technology)) {
                   topicIsKnown = true;
                   Integer topicIndex = requiredTopics.indexOf(technology);
                   if(topicIndex >= 0) {
                       requiredTopics.remove(topicIndex);
                   }
               }
            }
           
           if(topicIsKnown) {
               Interviewers_Interview__c interviewerInterview = new Interviewers_Interview__c(
                   Interview__c = interview.Id,
                   Interviewer__c = interviewer.Id,
                   Name=interview.Name + ' - ' + interviewer.Name
               );
               insert interviewerInterview;
               interviewerQuantity++;
           }
           //system.debug('interviewer ' + interviewerQuantity);
           //system.debug('requiredTopics ' + requiredTopics.isEmpty());
           if(interviewerQuantity >= 3 && requiredTopics.isEmpty()) {
               break;
           }
       }
   }

}